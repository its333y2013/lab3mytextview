package th.ac.tu.siit.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

public class MyTextView extends View {

	Paint p = new Paint();
	Rect bounds = new Rect();
	
	public MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		p.setAntiAlias(true);
		p.setColor(Color.BLUE);
		
		// Set text size to 24 dp
		p.setTextSize(dpToPixels(24.0f));
		
		// Set the origin of the string to be the bottom-left corner
		p.setTextAlign(Paint.Align.LEFT);
	}
	
	protected float dpToPixels(float dp) {
		return TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP,
                dp, 
                getResources().getDisplayMetrics());
	}
	

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		String s = "Hello World!";
		
		// Get the dimension of this view
		float viewWidth = this.getWidth();
		float viewHeight = this.getHeight();

		p.getTextBounds(s, 0, s.length(), bounds);
		
		canvas.drawText(s, //String to be drawn
				(viewWidth  - bounds.width())/2.0f,  //Left location of the string 
				(viewHeight + bounds.height())/2.0f, //Bottom location of the string
				p); //Paint object
	}
	
	public void setColor(int c) {
		p.setColor(c);
		this.invalidate();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			this.setBackgroundColor(Color.RED);
			this.invalidate();
			return true;
		}
		else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			this.setBackgroundColor(Color.YELLOW);
			this.invalidate();
			return true;
		}
		else if (event.getAction() == MotionEvent.ACTION_UP) {
			this.setBackgroundColor(Color.WHITE);
			this.invalidate();
			return true;
		}
		else {
			return super.onTouchEvent(event);
		}
	}
	
	
	
}
